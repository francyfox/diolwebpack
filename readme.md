# Diol webpack configuratuion
[![](https://img.shields.io/badge/npm-8.3.0-success)](https://img.shields.io/badge/npm-8.3.0-success) [![](https://img.shields.io/badge/yarn-1.22.17-success)](https://img.shields.io/badge/npm-8.3.0-success) 

Установите node package manager и yarn

    sudo apt install npm && apt install yarn
Первый запуск

    yarn
Режим разработки в live режиме

    yarn dev

Проход тестов

    yarn test

Компиляция финальной версии в папку dist

    yarn build

## Пользовательская настройка

В сборку добавлены scss<