const path = require('path')
const SimpleProgressWebpackPlugin = require('simple-progress-webpack-plugin')
const ColoredProgressBar = require('colored-progress-bar-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const SassLintPlugin = require('sass-lint-webpack')
const ESLintPlugin = require('eslint-webpack-plugin')
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin')  // для яблокодро**ов
const {DuplicatesPlugin} = require('inspectpack/plugin')
const webpack = require('webpack')

module.exports = {
    mode: 'development',
    devtool: 'inline-source-map',
    stats: 'errors-warnings',

    entry: {
        main: path.resolve(__dirname, '../src/index.js'),
        app: ['webpack-dev-server-status-bar']
    },
    output: {
        publicPath: '',
        path: path.resolve(__dirname, '../dist'),
        filename: '[name].bundle.js',
    },

    devServer: {
        client: {
            progress: true,
        },
        static: {
            directory: path.join(__dirname, '../src/'),
        },
        liveReload: true,
        historyApiFallback: true,
        open: true,
        compress: true,
        hot: true,
        port: 8091,
        allowedHosts: 'all'
    },

    plugins: [
        new ColoredProgressBar(),
        new SimpleProgressWebpackPlugin({
            format: 'minimal',
            name: ''
        }),
        new webpack.ProgressPlugin({
            activeModules: false,
            entries: true,
            modules: true,
            modulesCount: 5000,
            profile: false,
            dependencies: true,
            dependenciesCount: 10000,
            percentBy: null,
        }),
        require('autoprefixer'),
        new CleanWebpackPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            title: 'webpack Boilerplate',
            template: path.resolve(__dirname, '../src/template.html'), // шаблон
            filename: 'index.html', // название выходного файла
        }),
        new SassLintPlugin(),
        new ESLintPlugin(),
        new CaseSensitivePathsPlugin(),
        new DuplicatesPlugin()
    ],

    module: {
        rules: [
            // CSS, PostCSS, Sass
            {
                test: /\.(sass|scss|css)$/,
                use: [
                    "style-loader",
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                            importLoaders: 1,
                            modules: false
                        },
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            sourceMap: true
                        }
                    },
                ],
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader'],
            },
            {
                test: /\.(?:ico|gif|png|jpg|jpeg)$/i,
                type: 'asset/resource',
            },
            {
                test: /\.(woff(2)?|eot|ttf|otf|svg|)$/,
                type: 'asset/inline',
            },
        ],
    }
}