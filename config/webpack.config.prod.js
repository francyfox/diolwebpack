const path = require('path');

const MinimalClassnameGenerator = require('webpack-minimal-classnames')
const SimpleProgressWebpackPlugin = require('simple-progress-webpack-plugin')
const ColoredProgressBar = require('colored-progress-bar-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const HtmlInlineScriptPlugin = require('html-inline-script-webpack-plugin')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const MinifyPlugin = require("babel-minify-webpack-plugin")
const CssoWebpackPlugin = require('csso-webpack-plugin').default
// const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const WebpackObfuscator = require('webpack-obfuscator')
const SpeedMeasurePlugin = require("speed-measure-webpack-plugin")  // для тестов
const smp = new SpeedMeasurePlugin()
const webpack = require('webpack')

module.exports = smp.wrap({
    mode: 'production',

    entry: {
        main: path.resolve(__dirname, '../src/index.js'),
    },
    output: {
        publicPath: '',
        path: path.resolve(__dirname, '../dist'),
        filename: '[name].bundle.js',
    },

    optimization: {
        minimize: true,
        splitChunks: {
            chunks: 'async',
            minSize: 20000,
            minRemainingSize: 0,
            minChunks: 1,
            maxAsyncRequests: 30,
            maxInitialRequests: 30,
            enforceSizeThreshold: 50000,
            cacheGroups: {
                styles: {
                    name: 'styles',
                    test: /\.css$/,
                    chunks: 'all',
                    enforce: true
                }
            }
        }
    },

    devServer: {
        static: {
            directory: path.join(__dirname, '../src/'),
        },
        liveReload: true,
        historyApiFallback: true,
        open: true,
        compress: true,
        hot: true,
        port: 8091,
        allowedHosts: 'all'
    },

    plugins: [
        require('autoprefixer'),
        new ColoredProgressBar(),
        new SimpleProgressWebpackPlugin({
            format: 'minimal',
            name: ''
        }),
        new CleanWebpackPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            title: 'webpack Boilerplate',
            template: path.resolve(__dirname, '../src/template.html'), // шаблон
            filename: 'index.html', // название выходного файла
        }),
        new HtmlInlineScriptPlugin(),
        // new MiniCssExtractPlugin(),
        new WebpackObfuscator({
            rotateStringArray: true
        }),
        new CssoWebpackPlugin(),
        new MinifyPlugin()
    ],

    module: {
        rules: [
            // CSS, PostCSS, Sass
            {
                test: /\.(sass|scss|css)$/,
                use: [
                    'style-loader',
                    // MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            modules: {
                                getLocalIdent: MinimalClassnameGenerator({length: 1})
                            },
                        }
                    },
                    'postcss-loader',
                    'sass-loader'
                ],
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    'babel-loader',
                    {
                        loader: WebpackObfuscator.loader,
                        options: {
                            rotateStringArray: true
                        }
                    }
                ],
            },
            {
                test: /\.(?:ico|gif|png|jpg|jpeg)$/i,
                type: 'asset/resource',
            },
            {
                test: /\.(woff(2)?|eot|ttf|otf|svg|)$/,
                type: 'asset/inline',
            },
        ],
    }
})