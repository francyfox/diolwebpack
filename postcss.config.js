const purgecss = require('@fullhuman/postcss-purgecss');
const devMode = process.env.MODE !== "production";

const sourceContent = [
    './**/*.html',
    './**/*.md',
    './**/*.js'
];

module.exports = {
    plugins: [
        devMode ? '' : purgecss({
            content: sourceContent,
            variables: true,
            keyframes: true
        }),

        'postcss-discard-comments',
        'cssnano',
        {
            'postcss-preset-env': {
                browsers: 'last 2 versions',
            },
        }
    ]
};
